import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/models/response.dart';

class ReservationController {
  static getReservations(String id) async {
    var params = {'id': id};

    final url =
        Uri.parse('https://taxiproxi.fr/api2022/chauffeur-devis-attentes.php');
    final newUri = url.replace(queryParameters: params);
    final response = await http.get(newUri);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['code'] == 0) {
        return Response.fromJson(body);
      }

      if (body['result'] is! Map) {
        return body['result']
            .map((json) => Reservation.fromJson(json))
            .toList();
      }
    } else {
      throw Exception();
    }
  }

  static getPendingPropositions(String id) async {
    var params = {'id': id};

    final url = Uri.parse(
        'https://taxiproxi.fr/api2022/chauffeur-devis-propositions.php');
    final newUri = url.replace(queryParameters: params);
    final response = await http.get(newUri);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['code'] == 0) {
        return Response.fromJson(body);
      }

      if (body['result'] is! Map) {
        return body['result']
            .map((json) => Reservation.fromJson(json))
            .toList();
      }
    } else {
      throw Exception();
    }
  }

  static getAcceptedPropositions(String id) async {
    var params = {'id': id};

    final url =
        Uri.parse('https://taxiproxi.fr/api2022/chauffeur-devis-acceptes.php');
    final newUri = url.replace(queryParameters: params);
    final response = await http.get(newUri);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['code'] == 0) {
        return Response.fromJson(body);
      }
      if (body['result'] is! Map) {
        return body['result']
            .map((json) => Reservation.fromJson(json))
            .toList();
      }
    } else {
      throw Exception();
    }
  }
}
