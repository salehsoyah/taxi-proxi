import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:taxi_proxi/models/country.dart';
import 'package:taxi_proxi/models/response.dart';
import 'package:taxi_proxi/models/user.dart';

class UserController {
  static login(String username, password) async {
    var params = {'username': username, 'password': password};

    final url = Uri.parse('https://taxiproxi.fr/api2022/connect.php');
    final response = await http.post(url, body: params);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['code'] == 0) {
        return Response.fromJson(body);
      }
      return User.fromJson(body['result']);
    } else {
      throw Exception();
    }
  }

  static register(User user) async {
    final url = Uri.parse('https://taxiproxi.fr/api2022/inscrit.php');
    final response = await http.post(url, body: user.toJsonInscription());
      print(user.toJsonInscription());
      print(response.body);

  }

  static Future<List<Country>> getCountries() async {
    final url = Uri.parse('https://restcountries.com/v3.1/all');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      List countries = body;

      return countries =
          countries.map((json) => Country.fromJson(json)).toList();
    } else {
      throw Exception();
    }
  }
}
