class User {
  int? id;
  String? type;
  String? name;
  String? address;
  String? ville;
  String? nom;
  String? prenom;
  String? email;
  String? tel;
  double? solde;
  String? username;

  String? password;
  String? passwordConfirm;
  String? country;
  String? taxiName;
  String? registrationNumber;
  String? carType;
  String? brand;
  String? nbPlaces;
  String? animals;
  String? security;
  String? kids;
  String? cash;
  String? card;
  String? check;

  User(
      {this.id,
      this.type,
      this.name,
      this.address,
      this.email,
      this.ville,
      this.nom,
      this.prenom,
      this.solde,
      this.tel,
      this.username,
      this.password,
      this.passwordConfirm,
      this.country,
      this.taxiName,
      this.registrationNumber,
      this.carType,
      this.brand,
      this.nbPlaces,
      this.animals,
      this.security,
      this.kids,
      this.cash,
      this.card,
      this.check});

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json['id'] is int ? json['id'] : int.parse(json['id']),
        type: json['type'],
        name: json['name'],
        address: json['address'],
        email: json['email'],
        ville: json['ville'],
        nom: json['nom'],
        prenom: json['prenom'],
        solde: json['solde'] is double
            ? json['solde']
            : double.parse(json['solde']),
        tel: json['tel'],
        username: json['username'],
      );

  Map<String, dynamic> toJson() => {
        'username': username,
        'id': id,
        'solde': solde,
        'type': type,
        'name': name,
        'address': address,
        'email': email,
        'ville': ville,
        'nom': nom,
        'prenom': prenom,
        'tel': tel
      };

  Map<String, dynamic> toJsonInscription() => {
        'type': "CHAUFFEUR",
        'desc_license': registrationNumber,
        'password': password,
        'desc_place': nbPlaces,
        'nom': nom,
        'desc_paiement_es': cash,
        'email': email,
        'prenom': prenom,
        'desc_ss': security,
        'desc_sbb': kids,
        'repassword': passwordConfirm,
        'username': username,
        'tel': tel,
        'ville': ville,
        'name': taxiName,
        'desc_animaux': animals,
        'desc_vehicule': type,
        'logo': brand,
        'pays': country,
        'desc_paiement_cb': card,
      };
}
