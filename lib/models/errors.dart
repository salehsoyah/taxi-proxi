import 'package:flutter/cupertino.dart';

class Error extends ChangeNotifier{
  final List<String> errors = [];

  void addError(String error) {
    errors.add(error);
    notifyListeners();
  }

  void removeError(String error) {
    errors.remove(error);
    notifyListeners();
  }

  List<String> get errorsList{
    return errors;
  }


}