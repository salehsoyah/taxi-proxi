class Response {
  final int code;
  final String message;

  const Response({
    required this.code,
    required this.message,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        code: json['code'],
        message: json['message'],
      );
}
