class Reservation {
  final int id;
  final int places;
  final String departureAddress;
  final String arrivalAddress;
  final String note;
  final String reservationDate;
  final String reservationTime;
  final double distance;
  final String value;
  final String proposition;

  const Reservation(
      {required this.id,
      required this.places,
      required this.departureAddress,
      required this.arrivalAddress,
      required this.note,
      required this.reservationDate,
      required this.reservationTime,
      required this.distance,
      required this.value,
      required this.proposition});

  factory Reservation.fromJson(Map<String, dynamic> json) => Reservation(
      id: json['id_reservation'] is int
          ? json['id_reservation']
          : int.parse(json['id_reservation']),
      places: json['desc_place'] is int
          ? json['desc_place']
          : int.parse(json['desc_place']),
      departureAddress: json['adresse_depart'],
      arrivalAddress: json['adresse_arrivee'],
      note: json['note'],
      reservationDate: json['date_reservation'],
      reservationTime: json['heure_reservation'],
      distance: json['distance'] is double
          ? json['distance']
          : double.parse(json['distance']),
      value: json['valeur'] ?? '',
      proposition: json['proposition'] is int ? json['proposition'].toString() : json['proposition']);

  Map<String, dynamic> toJson() => {
        'id': id,
        'places': places,
        'departureAddress': departureAddress,
        'arrivalAddress': arrivalAddress,
        'note': note,
        'reservationDate': reservationDate,
        'reservationTime': reservationTime,
        'distance': distance,
        'value': value,
        'proposition': proposition
      };
}
