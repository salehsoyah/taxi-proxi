class Type {
  String? title;
  String? code;

  Type({this.title, this.code});
}
