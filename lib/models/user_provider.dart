import 'package:flutter/cupertino.dart';
import 'package:taxi_proxi/models/user.dart';

class UserProvider extends ChangeNotifier {
  User user =  User(
      prenom: '',
      id: 0,
      nom: '',
      solde: 0,
      email: '',
      address: '',
      username: '',
      type: '',
      tel: '',
      ville: '',
      name: '');

  void setCurrentUser(User newUser) {
    user = newUser;
    notifyListeners();
  }
}
