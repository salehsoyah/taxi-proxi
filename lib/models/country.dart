class Country{
  String? title;
  String? code;

  Country({this.title, this.code});


  factory Country.fromJson(Map<String, dynamic> json) => Country(
    title: json['name']['common'],
    code: json['cca2'] != null ? json['cca2'].toLowerCase() : '',
  );

}