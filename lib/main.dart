import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_proxi/models/errors.dart';
import 'package:taxi_proxi/models/user_provider.dart';
import 'package:taxi_proxi/views/forgot_password/forgot_password_screen.dart';
import 'package:taxi_proxi/views/home/home.dart';
import 'package:taxi_proxi/views/identification/identification.dart';
import 'package:taxi_proxi/views/login/sign_in_screen.dart';
import 'package:taxi_proxi/views/sign_up/components/sign_up_form.dart';
import 'package:taxi_proxi/views/sign_up/sign_up_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var status = sharedPreferences.getBool('isLoggedIn');
  // print(status);

  runApp(MyApp(
    widget: status == true ? const Home() : const SignInScreen(),
  ));
}

class MyApp extends StatelessWidget {
  Widget widget;

  MyApp({Key? key, required this.widget});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (BuildContext context) {
              return Error();
            },
          ),
          ChangeNotifierProvider(
            create: (BuildContext context) {
              return UserProvider();
            },
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: widget,
        ));
  }
}
