import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kNameNullError = "Veuillez saisir votre nom";
const String kShortNameError = "Le nom est trop court";

const String kUserNameNullError = "Veuillez saisir nom d'utilisateur";

const String kEmailNullError = "Veuillez saisir votre e-mail";
const String kInvalidEmailError = "Veuillez saisir une adresse e-mail valide";
const String kPassNullError = "S'il vous plait entrez votre mot de passe";
const String kShortPassError = "Le mot de passe est trop court";
const String kMatchPassError = "Les mots de passe ne correspondent pas";
const String kNamelNullError = "S'il vous plaît entrez votre nom";
const String kPhoneNumberNullError =
    "Veuillez entrer votre numéro de téléphone";
const String kAddressNullError = "Veuillez entrer votre adresse";
const String kPropositionNullError = "Veuillez entrer votre proposition";
const String kShortPropositionError = "Votre proposition et inférieur à ";
//inscription
const String kLoginNullError = "Veuillez saisir votre login";
const String kShortLoginError = "Le login est trop court";

const String kFamilyNameNullError = "Veuillez saisir votre nom";
const String kShortFamilyNameError = "Le nom est trop court";

const String kFirstNameNullError = "Veuillez saisir votre prénom";
const String kShortFirstNameError = "Le prénom est trop court";

const String kPhoneNullError = "Veuillez saisir votre numéro de téléphone";
const String kShortPhoneError = "Le numéro de téléphone est trop court";

const String kTownNullError = "Veuillez saisir votre ville";
const String kShortTownError = "La ville est trop courte";

const String kCountryNullError = "Veuillez choisir votre pays";

const String kTaxiNameNullError = "Veuillez saisir le nom du taxi";
const String kShortTaxiNameError = "Le nom du taxi est trop court";

const String kRegistrationNumberNullError = "Veuillez saisir l'immatricule";
const String kShortRegistrationNumberError = "L'immatricule est trop courte";

const String kTypeNullError = "Veuillez choisir le type du voiture";

const String kBrandNullError = "Veuillez choisir le modèle du voiture";

const String kNbPlacesNullError = "Veuillez saisir le nombre de places de la voiture";

