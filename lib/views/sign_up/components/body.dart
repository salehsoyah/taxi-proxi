import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/constants.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/login/sign_in_screen.dart';

import 'sign_up_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:  const BoxDecoration(
        color: Color(0xFF303030),
        image: DecorationImage(
          image: AssetImage("assets/images/map.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.paddingTop + getProportionateScreenHeight(5),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const SignInScreen()),
                    );
                  },
                  icon: const Icon(Icons.arrow_back_ios, color: Colors.white,)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.02), // 4%
                  const SignUpForm(),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  const Text(
                    'En poursuivant votre confirmation, vous acceptez \nnos termes et conditions',
                    textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Josefin Sans Regular',
                          color: Colors.white
                      )
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
