import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:taxi_proxi/controllers/user_controller.dart';
import 'package:taxi_proxi/models/brand.dart';
import 'package:taxi_proxi/models/country.dart';
import 'package:taxi_proxi/models/type.dart';
import 'package:taxi_proxi/models/user.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/default_button.dart';
import 'package:taxi_proxi/views/common/form_error.dart';
import '../../../utilities/constants.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  String? login;
  String? password;
  String? confirmPassword;
  String? email;
  String? phone;
  String? familyName;
  String? firstName;
  String? town;
  String? country;
  String? taxiName;
  String? registrationNumber;
  String? taxiTypeValue;
  String? taxiBrandValue;
  bool animals = false;
  bool security = false;
  bool kids = false;
  bool card = false;
  bool check = false;
  bool cash = false;
  bool generalConditions = false;
  double nbPlaces = 4;
  bool remember = false;
  final List<String?> errors = [];

  List<Country> countries = [];
  List<Type> types = [
    Type(title: "Berline", code: "Berline"),
    Type(title: "Break", code: "Break"),
    Type(title: "Monospace", code: "Monospace"),
  ];

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  Future initCountries() async {
    final countries = await UserController.getCountries();
    setState(() {
      this.countries = countries;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initCountries();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildLoginFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildConformPassFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildPhoneFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildFamilyNameFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildTownFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildCountryFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildTaxiNameFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildRegistrationNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildTaxiTypeFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildTaxiBrandFormField(),
          SizedBox(height: getProportionateScreenHeight(25)),
          buildPlacesNbrFormField(),
          Theme(
            data: Theme.of(context).copyWith(
                checkboxTheme: Theme.of(context)
                    .checkboxTheme
                    .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Animeaux",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: animals,
              onChanged: (val) {
                if (animals == false) {
                  setState(() {
                    animals = true;
                  });
                } else {
                  setState(() {
                    animals = false;
                  });
                }
              },
            ),
          ),
          Theme(
            data: Theme.of(context).copyWith(
                checkboxTheme: Theme.of(context)
                    .checkboxTheme
                    .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Sécurité sociale",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: security,
              onChanged: (val) {
                if (security == false) {
                  setState(() {
                    security = true;
                  });
                } else {
                  setState(() {
                    security = false;
                  });
                }
              },
            ),
          ),
          Theme(data: Theme.of(context).copyWith(
              checkboxTheme: Theme.of(context)
                  .checkboxTheme
                  .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Siège bébé",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: kids,
              onChanged: (val) {
                if (kids == false) {
                  setState(() {
                    kids = true;
                  });
                } else {
                  setState(() {
                    kids = false;
                  });
                }
              },
            ),
          ),
          const Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Mode de paiement à bord",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Josefin Sans Regular',
                    color: Colors.white),
              )),
          Theme(data: Theme.of(context).copyWith(
              checkboxTheme: Theme.of(context)
                  .checkboxTheme
                  .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Espèce",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: cash,
              onChanged: (val) {
                if (cash == false) {
                  setState(() {
                    cash = true;
                  });
                } else {
                  setState(() {
                    cash = false;
                  });
                }
              },
            ),
          ),
          Theme(data: Theme.of(context).copyWith(
              checkboxTheme: Theme.of(context)
                  .checkboxTheme
                  .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Carte bancaire",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: card,
              onChanged: (val) {
                if (card == false) {
                  setState(() {
                    card = true;
                  });
                } else {
                  setState(() {
                    card = false;
                  });
                }
              },
            ),
          ),
          Theme(data: Theme.of(context).copyWith(
              checkboxTheme: Theme.of(context)
                  .checkboxTheme
                  .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Chèque",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: check,
              onChanged: (val) {
                if (check == false) {
                  setState(() {
                    check = true;
                  });
                } else {
                  setState(() {
                    check = false;
                  });
                }
              },
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(25)),
          const Divider(
            thickness: 2,
          ),
          Theme(data: Theme.of(context).copyWith(
              checkboxTheme: Theme.of(context)
                  .checkboxTheme
                  .copyWith(side: const BorderSide(color: Colors.white))),
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.red,
              title: const Text("Accepter les conditions générales d'utilisation",
                  style: TextStyle(
                      fontFamily: 'Josefin Sans Regular', color: Colors.white)),
              value: generalConditions,
              onChanged: (val) {
                if (generalConditions == false) {
                  setState(() {
                    generalConditions = true;
                  });
                } else {
                  setState(() {
                    generalConditions = false;
                  });
                }
              },
            ),
          ),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            fontFamily: 'Josefin Sans',
            textColor: Colors.black,
            buttonColor: Colors.amber,
            text: "Inscription",
            press: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                if (generalConditions == true) {
                  User user = User();
                  user.registrationNumber = registrationNumber;
                  user.password = password;
                  user.nbPlaces = nbPlaces.toString();
                  user.nom = familyName;
                  user.cash = cash == true ? "ES" : "0";
                  user.email = email;
                  user.prenom = firstName;
                  user.security = security == true ? "1" : "0";
                  user.kids = kids == true ? "1" : "0";
                  user.passwordConfirm = confirmPassword;
                  user.username = login;
                  user.tel = phone;
                  user.ville = town;
                  user.taxiName = taxiName;
                  user.animals = animals == true ? "1" : "0";
                  user.type = taxiTypeValue;
                  user.brand = taxiBrandValue;
                  user.country = country;
                  user.card = card == true ? "CB" : "0";

                  UserController.register(user);
                } else {
                  displayToastMessage(
                      "Veuillez accepter la conditions générales", context);
                }
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildLoginFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => login = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kLoginNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortLoginError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kLoginNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortLoginError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: "Login",
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.account_circle,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        password = value;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        hintText: "Mot de passe",
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.lock_open_sharp,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildConformPassFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => confirmPassword = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.isNotEmpty && password == confirmPassword) {
          removeError(error: kMatchPassError);
        }
        confirmPassword = value;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if ((password != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: "Retapper votre mot de passe",
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.lock_open_sharp,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        hintText: "Email",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.mail_outline_sharp,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phone = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortPhoneError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPhoneNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortPhoneError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        hintText: "Téléphone",
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.phone_android,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildFamilyNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => familyName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kFamilyNameNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortFamilyNameError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kFamilyNameNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortFamilyNameError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        hintText: "Nom",
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.family_restroom_sharp,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => firstName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kFirstNameNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortFirstNameError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kFirstNameNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortFirstNameError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        fillColor: Colors.white.withOpacity(0.2),
        filled: true,
        hintText: "Entrer votre prénom",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.person,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildTownFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => town = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kTownNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortTownError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kTownNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortTownError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        fillColor: Colors.white.withOpacity(0.2),
        filled: true,
        hintText: "Entrer votre ville",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.account_balance_rounded,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
  }

  DropdownButtonFormField buildCountryFormField() {
    return DropdownButtonFormField(
      iconDisabledColor: Colors.white,
      iconEnabledColor: Colors.white,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
      ),
      validator: (value) {
        if (value == null) {
          addError(error: kCountryNullError);
          return "";
        }
        return null;
      },
      hint: const Text(
        "Pays",
        style:
            TextStyle(fontFamily: 'Josefin Sans Regular', color: Colors.white),
      ),
      isExpanded: true,
      // Initial Value
      value: country,
      // Down Arrow Icon
      icon: const Icon(Icons.keyboard_arrow_down),
      // Array list of items
      items: countries.map((Country country) {
        return DropdownMenuItem(
          value: country.code,
          child: Text(country.title!),
        );
      }).toList(),
      // After selecting the desired option,it will
      // change button value to selected value
      onChanged: (newValue) {
        setState(() {
          country = newValue.toString();
        });
        if (newValue != null) {
          removeError(error: kCountryNullError);
        }
        return;
      },
    );
  }

  TextFormField buildTaxiNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => taxiName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kTaxiNameNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortTaxiNameError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kTaxiNameNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortTaxiNameError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        fillColor: Colors.white.withOpacity(0.2),
        filled: true,
        hintText: "Entrer le nom du taxi",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.local_taxi_sharp,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  TextFormField buildRegistrationNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => registrationNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kRegistrationNumberNullError);
        } else if (value.length >= 3) {
          removeError(error: kShortRegistrationNumberError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kRegistrationNumberNullError);
          return "";
        } else if (value.length < 3) {
          addError(error: kShortRegistrationNumberError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        fillColor: Colors.white.withOpacity(0.2),
        filled: true,
        hintText: "Entrer l'immatriculation",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.confirmation_number,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }

  DropdownButtonFormField buildTaxiTypeFormField() {
    return DropdownButtonFormField(
      iconDisabledColor: Colors.white,
      iconEnabledColor: Colors.white,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
      ),
      validator: (value) {
        if (value == null) {
          addError(error: kTypeNullError);
          return "";
        }
        return null;
      },
      hint: const Text(
        "Type véhicule",
        style:
            TextStyle(fontFamily: 'Josefin Sans Regular', color: Colors.white),
      ),
      isExpanded: true,
      // Initial Value
      value: taxiTypeValue,
      // Down Arrow Icon
      icon: const Icon(Icons.keyboard_arrow_down),
      // Array list of items
      items: types.map((Type type) {
        return DropdownMenuItem(
          value: type.code,
          child: Text(type.title!),
        );
      }).toList(),
      // After selecting the desired option,it will
      // change button value to selected value
      onChanged: (newValue) {
        setState(() {
          taxiTypeValue = newValue.toString();
        });
        if (newValue != null) {
          removeError(error: kTypeNullError);
        }
        return;
      },
    );
  }

  DropdownButtonFormField buildTaxiBrandFormField() {
    return DropdownButtonFormField(
      iconDisabledColor: Colors.white,
      iconEnabledColor: Colors.white,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white.withOpacity(0.2),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
      ),
      validator: (value) {
        if (value == null) {
          addError(error: kBrandNullError);
          return "";
        }
        return null;
      },
      hint: const Text(
        "Marque véhicule",
        style:
            TextStyle(fontFamily: 'Josefin Sans Regular', color: Colors.white),
      ),
      isExpanded: true,
      // Initial Value
      value: taxiBrandValue,

      // Down Arrow Icon
      icon: const Icon(Icons.keyboard_arrow_down),

      // Array list of items
      items: Brand.brands.map((Brand brand) {
        return DropdownMenuItem(
          value: brand.code,
          child: Text(brand.title!),
        );
      }).toList(),
      // After selecting the desired option,it will
      // change button value to selected value
      onChanged: (newValue) {
        setState(() {
          taxiBrandValue = newValue.toString();
        });
        if (newValue != null) {
          removeError(error: kBrandNullError);
        }
        return;
      },
    );
  }

  SpinBox buildPlacesNbrFormField() {
    return SpinBox(
      validator: (value) {
        if (value == null) {
          addError(error: kNbPlacesNullError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        setState(() {
          nbPlaces = value;
        });
        if (!value.isNaN) {
          removeError(error: kRegistrationNumberNullError);
        }
        return;
      },
      min: 1,
      max: 9,
      value: nbPlaces,
      spacing: 5,
      direction: Axis.horizontal,
      textStyle: const TextStyle(
          fontSize: 15,
          color: Colors.white,
          fontFamily: 'Josefin Sans Regular'),
      incrementIcon: const Icon(
        Icons.exposure_plus_1,
        size: 20,
        color: Colors.white,
      ),
      decrementIcon: const Icon(
        Icons.exposure_minus_1_rounded,
        size: 20,
        color: Colors.white,
      ),
      decoration: InputDecoration(
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        fillColor: Colors.white.withOpacity(0.2),
        filled: true,
        labelText: "Nombre de places",
        labelStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
}
