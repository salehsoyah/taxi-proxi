import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/accepted_proposal/components/body.dart';
import 'package:taxi_proxi/views/common/appbar.dart';

class ProposalsAccepted extends StatefulWidget {
  const ProposalsAccepted({Key? key}) : super(key: key);

  @override
  _ProposalsAcceptedState createState() => _ProposalsAcceptedState();
}

class _ProposalsAcceptedState extends State<ProposalsAccepted> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      appBar: Appbar(),
      body: Body(),
    );
  }
}
