import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_proxi/controllers/reservations_controller.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/models/user.dart';
import 'package:taxi_proxi/models/user_provider.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/accepted_proposal/components/proposals.dart';
import 'package:taxi_proxi/views/common/progress_dialog.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  var now = DateTime.now();
  var formatter = DateFormat('dd/MM/yyyy');
  final Future<SharedPreferences> preferences = SharedPreferences.getInstance();
  List<Reservation> acceptedPropositions = [];

  getUser() async {
    final SharedPreferences prefs = await preferences;
    var user = prefs.getString('user');
    return jsonDecode(user!);
  }

  @override
  void initState() {
    getUser().then((usr) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
            ProgressDialog(message: "Chargement ..."),
      );
      ReservationController.getAcceptedPropositions(
              User.fromJson(usr).id.toString())
          .then((rsv) {
        Navigator.pop(context);
        if (rsv is List<dynamic>) {
          setState(() {
            acceptedPropositions = rsv.cast<Reservation>();
          });
        }
      });
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(15),
                    bottomLeft: Radius.circular(15)),
                gradient: const LinearGradient(
                    colors: [Color(0xFFd00000), Colors.yellow],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    tileMode: TileMode.clamp),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.5),
                    offset: const Offset(0.0, 5.0), //(x,y)
                    blurRadius: 7.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 5, right: 8.0, left: 8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.notes,
                              size: getProportionateScreenWidth(25),
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: getProportionateScreenWidth(15),
                            ),
                            Consumer<UserProvider>(
                              builder: (context, userProvider, child) {
                                return Text(
                                  "Bonjour " + userProvider.user.username!,
                                  style: TextStyle(
                                      fontSize: getProportionateScreenWidth(18),
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontFamily: 'Jost'),
                                );
                              },
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              formatter.format(now).toString(),
                              style: TextStyle(
                                  fontSize: getProportionateScreenWidth(13),
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                  fontFamily: 'Jost'),
                            ),
                            SizedBox(
                              width: getProportionateScreenWidth(15),
                            ),
                            CircleAvatar(
                              radius: getProportionateScreenWidth(30),
                              backgroundImage:
                                  const AssetImage('assets/images/avatar.png'),
                            ),
                          ],
                        )
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        Consumer<UserProvider>(
                          builder: (context, userProvider, child) {
                            return Text(
                              "\$" + userProvider.user.solde.toString(),
                              style: TextStyle(
                                  fontSize: getProportionateScreenWidth(30),
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Jost'),
                            );
                          },
                        ),
                        Text(
                          "Solde actuel",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(18),
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontFamily: 'Jost'),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(5),
                        ),
                        Text(
                          "Propositions acceptées",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(20),
                              fontWeight: FontWeight.bold,
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(5),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            acceptedPropositions.isNotEmpty
                ? Proposals(
                    accepted: acceptedPropositions,
                  )
                : Column(
                    children: [
                      SizedBox(
                        height: getProportionateScreenHeight(230),
                      ),
                      const Text("Pas de proposition acceptée"),
                    ],
                  )
          ], //<Widget>[]
        ),
      ),
    );
  }
}
