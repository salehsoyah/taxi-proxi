import 'package:flutter/material.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/accepted_proposal/components/proposals_accepted_card.dart';
import 'package:taxi_proxi/views/home/components/proposition_card.dart';
import 'package:taxi_proxi/views/pending_proposals/components/proposals_pending_card.dart';
import 'package:taxi_proxi/views/reservations_pending/components/reservation_card.dart';

class Proposals extends StatefulWidget {
  Proposals({Key? key, required this.accepted}) : super(key: key);
  List<Reservation> accepted;

  @override
  _ProposalsState createState() => _ProposalsState();
}

class _ProposalsState extends State<Proposals> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.all(getProportionateScreenWidth(8)),
        child: ListView.builder(
            itemCount: widget.accepted.length,
            itemBuilder: (BuildContext context, int index) {
              return ProposalsAcceptedCard(
                press: () {},
                acceptedObject: widget.accepted[index],
              );
            }),
      ),
    );
  }
}
