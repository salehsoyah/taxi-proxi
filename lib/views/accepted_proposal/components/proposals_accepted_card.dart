import 'package:flutter/material.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class ProposalsAcceptedCard extends StatefulWidget {
  final GestureTapCallback press;
  final Reservation acceptedObject;

  const ProposalsAcceptedCard(
      {Key? key, required this.press, required this.acceptedObject})
      : super(key: key);

  @override
  _ProposalsAcceptedCardState createState() => _ProposalsAcceptedCardState();
}

class _ProposalsAcceptedCardState extends State<ProposalsAcceptedCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(getProportionateScreenWidth(10)),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
        gradient: const LinearGradient(
            colors: [Color(0xFFd00000), Color(0xFFd00000)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            tileMode: TileMode.clamp),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            offset: const Offset(0.0, 5.0), //(x,y)
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: getProportionateScreenWidth(8)),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  const Icon(
                    Icons.directions_car,
                    color: Colors.white,
                  ),
                  SizedBox(width: getProportionateScreenWidth(4)),
                  Flexible(
                    child: Text(
                      "Votre proposition " +
                          widget.acceptedObject.proposition.toString() +
                          "\$",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Jost'),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                    top: getProportionateScreenWidth(3),
                    bottom: getProportionateScreenWidth(3)),
                padding: EdgeInsets.all(getProportionateScreenWidth(2)),
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(4)),
                child: Text("Acceptée",
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(14),
                        color: Colors.white,
                        fontFamily: 'Jost',
                        fontWeight: FontWeight.bold)),
              ),
              Row(
                children: [
                  Icon(
                    Icons.emoji_people,
                    size: getProportionateScreenWidth(20),
                    color: Colors.white,
                  ),
                  Text(
                    widget.acceptedObject.places.toString() + ' Places',
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(12),
                        color: Colors.white70,
                        fontFamily: 'Jost'),
                  ),
                ],
              ),
              SizedBox(height: getProportionateScreenWidth(3)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse de départ",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.acceptedObject.departureAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse d'arrivé",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.acceptedObject.arrivalAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.watch_later_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Date de la course",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.acceptedObject.reservationDate,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
            ],
          )),
          Padding(
            padding: EdgeInsets.only(
                left: getProportionateScreenWidth(10),
                right: getProportionateScreenWidth(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: [
                    const Icon(
                      Icons.map_sharp,
                      color: Colors.white,
                    ),
                    Text(
                      widget.acceptedObject.distance.toString() + " Km",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontFamily: 'Jost'),
                    ),
                  ],
                ),
                Text(
                  'Distance',
                  style: TextStyle(
                      fontSize: getProportionateScreenWidth(14),
                      color: Colors.white70,
                      fontFamily: 'Jost'),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
