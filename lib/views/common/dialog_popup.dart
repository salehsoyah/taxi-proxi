import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/default_button.dart';

class DialogPopUp extends StatelessWidget {
  DialogPopUp({Key? key, required this.message}) : super(key: key);
  String message;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Container(
                      padding: const EdgeInsets.all(2),
                      color: const Color(0xFFd00000),
                      child: const Icon(
                        Icons.clear,
                        color: Colors.white,
                      )),
                )),
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text("Erreur",
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Jost',
                  fontWeight: FontWeight.bold,
                  fontSize: getProportionateScreenWidth(20))),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text(
            message + '.',
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Jost',
                fontSize: getProportionateScreenWidth(16)),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: getProportionateScreenHeight(15),
          ),
          DefaultButton(
            text: "Fermer",
            press: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
