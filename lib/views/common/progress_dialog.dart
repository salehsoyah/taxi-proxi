import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class ProgressDialog extends StatelessWidget {
  String message;

  ProgressDialog({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: const Color(0xFFFFCF02),
      child: Container(
        margin: const EdgeInsets.all(5.0),
        width: double.infinity,
        decoration: BoxDecoration( 
          color: Colors.white,
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(getProportionateScreenWidth(15)),
          child: Row(
            children: [
              SizedBox(
                width: getProportionateScreenWidth(7),
              ),
              const CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
              ),
              SizedBox(
                width: getProportionateScreenWidth(24),
              ),
              Flexible(
                child: Text(
                  message,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: getProportionateScreenWidth(13),
                      fontFamily: 'Jost'),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
