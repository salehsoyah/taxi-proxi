import 'package:flutter/material.dart';

class CheckBoxFormField extends StatefulWidget {
  CheckBoxFormField({Key? key, required this.title, required this.value}) : super(key: key);
  String title;
  bool value;
  @override
  _CheckBoxFormFieldState createState() => _CheckBoxFormFieldState();
}

class _CheckBoxFormFieldState extends State<CheckBoxFormField> {
  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      checkColor: Colors.white,
      activeColor: Colors.red,
      title: Text(widget.title),
      value: widget.value,
      onChanged: (val) {
        if(widget.value == false){
          setState(() {
            widget.value = true;
            print(widget.value);
          });
        }else{
          setState(() {
            widget.value = false;
            print(widget.value);
          });
        }
      },
    );
  }
}