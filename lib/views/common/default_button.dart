import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';


class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    this.text,
    this.press,
    this.buttonColor,
    this.textColor,
    this.fontFamily
  }) : super(key: key);
  final String? text;
  final Function? press;
  final Color? textColor;
  final Color? buttonColor;
  final String? fontFamily;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(56),
      child: TextButton(
        style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
          primary: Colors.white,
          backgroundColor: buttonColor,
          elevation: 10
        ),
        onPressed: press as void Function()?,
        child: Text(
          text!,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(17),
            color: textColor,
              fontFamily: fontFamily,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
}
