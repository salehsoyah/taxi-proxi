import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class FormError extends StatelessWidget {
  const FormError({
    Key? key,
    required this.errors,
  }) : super(key: key);

  final List<String?> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length, (index) => formErrorText(error: errors[index]!)),
    );
  }

  Row formErrorText({required String error}) {
    return Row(
      children: [
        Icon(
          Icons.error_outline_sharp,
          size: getProportionateScreenWidth(14),
          color: const Color(0xFFd00000),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Expanded(
          child: Text(
            error,
            style: const TextStyle(
              color: Color(0xFFd00000),
              fontFamily: 'Jost',
            ),
          ),
        ),
      ],
    );
  }
}
