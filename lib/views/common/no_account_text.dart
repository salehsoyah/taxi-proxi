import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/sign_up/sign_up_screen.dart';

class NoAccountText extends StatelessWidget {
  const NoAccountText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Vous n'avez pas de compte? ",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(17),
            fontFamily: 'Josefin Sans',
            color: Colors.white
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const SignUpScreen()),
            );
          },
          child: Text(
            "S'inscrire",
            style: TextStyle(
                decoration: TextDecoration.underline,
                fontSize: getProportionateScreenWidth(17),
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: 'Josefin Sans'),
          ),
        ),
      ],
    );
  }
}
