import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class Appbar extends StatelessWidget with PreferredSizeWidget {
  const Appbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [Colors.red, Colors.yellow]),
        ),
      ),
      preferredSize: Size(MediaQuery.of(context).size.width, 0),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(0);
}
