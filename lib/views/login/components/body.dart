import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/no_account_text.dart';
import 'sign_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage("assets/images/map.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.paddingTop + getProportionateScreenHeight(35),
            ),
            Image.asset(
              'assets/images/yellow.png',
              width: getProportionateScreenWidth(180),
            ),
            Stack(
              children: [
                Positioned(
                  top: 20,
                    left: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(),
                      child: Image.asset("assets/images/bot2.png"),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 35.0),
                  child: Column(
                    children: [
                      SizedBox(height: SizeConfig.screenHeight * 0.09),
                      const SignForm(),
                      const NoAccountText(),
                      const SizedBox(height: 60,)
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
