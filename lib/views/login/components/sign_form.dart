import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_proxi/controllers/user_controller.dart';
import 'package:taxi_proxi/models/response.dart';
import 'package:taxi_proxi/models/user.dart';
import 'package:taxi_proxi/utilities/constants.dart';
import 'package:taxi_proxi/utilities/constants.dart';
import 'package:taxi_proxi/utilities/constants.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/dialog_popup.dart';
import 'package:taxi_proxi/views/common/progress_dialog.dart';
import 'package:taxi_proxi/views/forgot_password/forgot_password_screen.dart';
import 'package:taxi_proxi/views/home/home.dart';
import 'package:taxi_proxi/views/common/default_button.dart';
import 'package:taxi_proxi/views/common/form_error.dart';
import 'package:taxi_proxi/views/sign_up/sign_up_screen.dart';

class SignForm extends StatefulWidget {
  const SignForm({Key? key}) : super(key: key);

  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  Future<SharedPreferences> preferences = SharedPreferences.getInstance();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  bool? remember = false;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error)) {
      setState(() {
        errors.add(error);
      });
    }
  }

  static void hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  void removeError({String? error}) {
    if (errors.contains(error)) {
      setState(() {
        errors.remove(error);
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenWidth(25)),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            buildEmailFormField(),
            SizedBox(height: getProportionateScreenHeight(15)),
            buildPasswordFormField(),
            SizedBox(height: getProportionateScreenHeight(15)),
            Row(
              children: [
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ForgotPasswordScreen()),
                    );
                  },
                  child: Text(
                    "Mot de passe oublié ?",
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(17),
                        fontFamily: 'Josefin Sans Regular',
                        color: Colors.white),
                  ),
                )
              ],
            ),
            FormError(errors: errors),
            SizedBox(height: getProportionateScreenHeight(40)),
            DefaultButton(
              fontFamily: 'Josefin Sans',
              textColor: Colors.black,
              buttonColor: Colors.amber,
              text: "Continuer",
              press: () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  login(usernameController.text, passwordController.text);
                  hideKeyboard(context);
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => const Home()),
                  // );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      controller: passwordController,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        fillColor: const Color(0xFF707070),
        filled: true,
        hintText: "Mot de passe",
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.lock_outline_rounded,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: usernameController,
      keyboardType: TextInputType.emailAddress,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kUserNameNullError);
        }
        return;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kUserNameNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        fillColor: const Color(0xFF707070),
        filled: true,
        hintText: "Nom d'utilisateur",
        hintStyle: TextStyle(
            color: Colors.white,
            fontFamily: 'Josefin Sans Regular',
            fontSize: getProportionateScreenWidth(17)),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        prefixIcon: const Icon(
          Icons.person,
          color: Colors.white,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }

  login(String username, password) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) =>
          ProgressDialog(message: "Authentification, veuillez patienter ..."),
    );
    UserController.login(username, password).then((value) async {
      Navigator.pop(context);
      if (value is Response) {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return DialogPopUp(
              message: value.message,
            );
          },
        );
      } else if (value is User) {
        final SharedPreferences prefs = await preferences;
        var user = jsonEncode(value.toJson());
        prefs.setString('user', user);
        prefs.setBool("isLoggedIn", true);

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Home()),
        );
      }
    });
  }
}
