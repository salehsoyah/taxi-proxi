import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_proxi/controllers/reservations_controller.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/models/user.dart';
import 'package:taxi_proxi/models/user_provider.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/progress_dialog.dart';
import 'package:taxi_proxi/views/home/components/propositions.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  var now = DateTime.now();
  var formatter = DateFormat('dd/MM/yyyy');
  final Future<SharedPreferences> preferences = SharedPreferences.getInstance();
  int reservations = 0;
  int pending = 0;
  int accepted = 0;

  getUser() async {
    final SharedPreferences prefs = await preferences;
    var user = prefs.getString('user');
    return jsonDecode(user!);
  }

  @override
  void initState() {
    getUser().then((usr) {
      Provider.of<UserProvider>(context, listen: false)
          .setCurrentUser(User.fromJson(usr));
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
            ProgressDialog(message: "Chargement ..."),
      );
      ReservationController.getReservations(User.fromJson(usr).id.toString())
          .then((rsv) {
        Navigator.pop(context);
        if (rsv is List<dynamic>) {
          setState(() {
            reservations = rsv.cast<Reservation>().length;
          });
        }
      });
      ReservationController.getPendingPropositions(
              User.fromJson(usr).id.toString())
          .then((rsv) {
        if (rsv is List<dynamic>) {
          setState(() {
            pending = rsv.cast<Reservation>().length;
          });
        }
      });
      ReservationController.getAcceptedPropositions(
              User.fromJson(usr).id.toString())
          .then((rsv) {
        if (rsv is List<dynamic>) {
          setState(() {
            accepted = rsv.cast<Reservation>().length;
          });
        }
      });
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          color: Color(0xFF303030),
          image: DecorationImage(
            image: AssetImage("assets/images/map.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(
              top: getProportionateScreenWidth(5), right: 8.0, left: 8.0),
          child: Column(
            children: [
              SizedBox(
                height: SizeConfig.paddingTop + 5,
              ),
              Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(25)),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.withOpacity(0.5)),
                  child: Image.asset("assets/images/pers.png")),
              Column(
                children: [
                  SizedBox(
                    height: getProportionateScreenHeight(15),
                  ),
                  Text(
                    "BONJOUR ${Provider.of<UserProvider>(context, listen: false).user.username}",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Josefin Sans',
                        fontWeight: FontWeight.bold,
                        fontSize: getProportionateScreenWidth(23)),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(5),
                  ),
                  Text(
                    formatter.format(now).toString(),
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Josefin Sans',
                        fontWeight: FontWeight.bold,
                        fontSize: getProportionateScreenWidth(23)),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(35),
                  ),
                  Consumer<UserProvider>(
                    builder: (context, userProvider, child) {
                      return Text(
                        "\$" + userProvider.user.solde.toString(),
                        style: TextStyle(
                          fontSize: getProportionateScreenWidth(62),
                          fontWeight: FontWeight.bold,
                          color: const Color(0xFFFFCF02),
                          fontFamily: 'Josefin Sans Medium',
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(2),
                  ),
                  Text(
                    "Solde actuel",
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(39),
                        color: Colors.white,
                        fontFamily: 'Josefin Sans Medium'),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(35),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/reservation2.png",
                          width: getProportionateScreenWidth(90),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(5),
                        ),
                        Flexible(
                          child: Text(
                            "Devis & Réservations en attentes",
                            style: TextStyle(
                              fontSize: getProportionateScreenWidth(20),
                              fontFamily: 'Josefin Sans Regular',
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text: reservations.toString(),
                                style: TextStyle(
                                    color: const Color(0xFFFFCF02),
                                    fontFamily: 'Josefin Sans Regular',
                                    fontSize: getProportionateScreenWidth(20))),
                            WidgetSpan(
                              child: Transform.translate(
                                offset: const Offset(2, -5),
                                child: Text(
                                  'TOTAL',
                                  //superscript is usually smaller in size
                                  textScaleFactor: 0.7,
                                  style: TextStyle(
                                    fontSize: getProportionateScreenWidth(20),
                                    fontFamily: 'Josefin Sans Regular',
                                    color: const Color(0xFFFFCF02)
                                  ),
                                ),
                              ),
                            )
                          ]),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(25),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/pendingProp.png",
                          width: getProportionateScreenWidth(90),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(5),
                        ),
                        Flexible(
                          child: Text(
                            "Mes Propositions en attentes",
                            style: TextStyle(
                              fontSize: getProportionateScreenWidth(20),
                              fontFamily: 'Josefin Sans Regular',
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text: pending.toString(),
                                style: TextStyle(
                                    color: const Color(0xFFFFCF02),
                                    fontFamily: 'Josefin Sans Regular',
                                    fontSize: getProportionateScreenWidth(20))),
                            WidgetSpan(
                              child: Transform.translate(
                                offset: const Offset(2, -5),
                                child: Text(
                                  'TOTAL',
                                  //superscript is usually smaller in size
                                  textScaleFactor: 0.7,
                                  style: TextStyle(
                                    fontSize: getProportionateScreenWidth(20),
                                    fontFamily: 'Josefin Sans Regular',
                                    color: const Color(0xFFFFCF02),
                                  ),
                                ),
                              ),
                            )
                          ]),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(25),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/propAccep.png",
                          width: getProportionateScreenWidth(90),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(5),
                        ),
                        Flexible(
                          child: Text(
                            "Mes Propositions acceptées",
                            style: TextStyle(
                              fontSize: getProportionateScreenWidth(20),
                              fontFamily: 'Josefin Sans Regular',
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text: accepted.toString(),
                                style: TextStyle(
                                    color: const Color(0xFFFFCF02),
                                    fontFamily: 'Josefin Sans Regular',
                                    fontSize: getProportionateScreenWidth(20))),
                            WidgetSpan(
                              child: Transform.translate(
                                offset: const Offset(2, -5),
                                child: Text(
                                  'TOTAL',
                                  //superscript is usually smaller in size
                                  textScaleFactor: 0.7,
                                  style: TextStyle(
                                    fontSize: getProportionateScreenWidth(20),
                                    fontFamily: 'Josefin Sans Regular',
                                    color: const Color(0xFFFFCF02),
                                  ),
                                ),
                              ),
                            )
                          ]),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
