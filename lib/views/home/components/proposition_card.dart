import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class PropositionCard extends StatefulWidget {
  final String category, image;
  final int numOfBrands;
  final GestureTapCallback press;

  const PropositionCard({
    Key? key,
    required this.category,
    required this.image,
    required this.numOfBrands,
    required this.press,
  }) : super(key: key);

  @override
  _PropositionCardState createState() => _PropositionCardState();
}

class _PropositionCardState extends State<PropositionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: (SizeConfig.screenHeight - SizeConfig.paddingTop) / 5.5,
      margin: EdgeInsets.all(getProportionateScreenWidth(10)),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
        gradient: const LinearGradient(
            colors: [Color(0xFFd00000), Color(0xFFd00000)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            tileMode: TileMode.clamp),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            offset: const Offset(0.0, 5.0), //(x,y)
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(10),
                  right: getProportionateScreenWidth(10)),
              child: CircleAvatar(
                backgroundColor: Colors.white.withOpacity(0.55),
                radius: getProportionateScreenWidth(50),
                backgroundImage: AssetImage(widget.image),
              )),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.category,
                style: TextStyle(
                    fontSize: getProportionateScreenWidth(18),
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Jost'),
                textAlign: TextAlign.start,
              ),
            ],
          )),
          Padding(
              padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(10),
                  right: getProportionateScreenWidth(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.numOfBrands.toString(),
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(18),
                        color: Colors.white,
                        fontFamily: 'Jost'),
                  ),
                  const Text(
                    'Total',
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontFamily: 'Jost'),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
