import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/accepted_proposal/proposals_accepted.dart';
import 'package:taxi_proxi/views/home/components/proposition_card.dart';
import 'package:taxi_proxi/views/pending_proposals/proposals_pending.dart';
import 'package:taxi_proxi/views/reservations_pending/reservations_pending.dart';

class Propositions extends StatefulWidget {
  Propositions(
      {Key? key,
      required this.reservations,
      required this.pendings,
      required this.accepted})
      : super(key: key);
  int reservations;
  int pendings;
  int accepted;

  @override
  _PropositionsState createState() => _PropositionsState();
}

class _PropositionsState extends State<Propositions> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenWidth(8)),
      child: Column(
        children: [
          ListView(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ReservationsPending()),
                  );
                },
                child: PropositionCard(
                  image: "assets/images/reservation.png",
                  category: "Devis & Réservation en attente",
                  numOfBrands: widget.reservations,
                  press: () {},
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ProposalsPending()),
                  );
                },
                child: PropositionCard(
                  image: "assets/images/wait.png",
                  category: "Mes Propositions en attente",
                  numOfBrands: widget.pendings,
                  press: () {},
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ProposalsAccepted()),
                  );
                },
                child: PropositionCard(
                  image: "assets/images/accepted.png",
                  category: "Mes Propositions acceptées",
                  numOfBrands: widget.accepted,
                  press: () {},
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
