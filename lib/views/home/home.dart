import 'package:flutter/material.dart';
import 'package:taxi_proxi/controllers/reservations_controller.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/appbar.dart';
import 'package:taxi_proxi/views/home/components/body.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: Body(),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: const <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Accueil'),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.person_pin), label: 'Profil'),
      //   ],
      //   elevation: 9,
      //   selectedItemColor: Colors.red,
      //   unselectedItemColor: Colors.black,
      //   selectedLabelStyle: TextStyle(
      //       fontSize: getProportionateScreenWidth(14),
      //       color: Colors.white,
      //       fontFamily: 'Jost'),
      //   unselectedLabelStyle: TextStyle(
      //       fontSize: getProportionateScreenWidth(14),
      //       color: Colors.white,
      //       fontFamily: 'Jost'),
      // ),
    );
  }
}
