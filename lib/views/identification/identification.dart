import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/default_button.dart';
import 'package:taxi_proxi/views/login/sign_in_screen.dart';
import 'package:taxi_proxi/views/sign_up/sign_up_screen.dart';

class Identification extends StatefulWidget {
  const Identification({Key? key}) : super(key: key);

  @override
  _IdentificationState createState() => _IdentificationState();
}

class _IdentificationState extends State<Identification> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: MediaQuery.of(context).padding.top,
          ),
          Align(
              alignment: Alignment.topRight,
              child: Image.asset(
                "assets/images/top.png",
                width: 120,
              )),
          Stack(
            children: [
              Positioned(
                top: 30,
                child: Image.asset(
                  "assets/images/carre.png",
                ),
                width: 400,
              ),
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  "assets/images/logo.png",
                  width: 110,
                ),
              )
            ],
          ),
          Expanded(
            child: Stack(
              children: [
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Image.asset(
                      "assets/images/bot.png",
                      width: 360,
                    )),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(90, 150, 90, 50),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        DefaultButton(
                          textColor: Colors.white,
                          buttonColor: Colors.red,
                          fontFamily: 'Montserrat SemiBold',
                          text: "Sign Up",
                          press: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignUpScreen()),
                            );
                          },
                        ),
                        const SizedBox(height: 20,),
                        DefaultButton(
                          fontFamily: 'Montserrat SemiBold',
                          textColor: Colors.red,
                          buttonColor: Colors.white,
                          text: "Login",
                          press: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignInScreen()),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
