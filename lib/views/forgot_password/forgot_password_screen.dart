import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

import 'components/body.dart';

class ForgotPasswordScreen extends StatelessWidget {

  const ForgotPasswordScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
