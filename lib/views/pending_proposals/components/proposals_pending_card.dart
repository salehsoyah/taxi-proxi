import 'package:flutter/material.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/utilities/size_config.dart';

class ProposalsPendingCard extends StatefulWidget {
  final GestureTapCallback press;
  final Reservation pendingObject;

  const ProposalsPendingCard({
    Key? key,
    required this.pendingObject,
    required this.press,
  }) : super(key: key);

  @override
  _ProposalsPendingCardState createState() => _ProposalsPendingCardState();
}

class _ProposalsPendingCardState extends State<ProposalsPendingCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(getProportionateScreenWidth(10)),
      padding: EdgeInsets.only(top: getProportionateScreenWidth(2)),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
        gradient: const LinearGradient(
            colors: [Color(0xFFd00000), Color(0xFFd00000)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            tileMode: TileMode.clamp),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            offset: const Offset(0.0, 5.0), //(x,y)
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: getProportionateScreenWidth(8)),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  const Icon(
                    Icons.directions_car,
                    color: Colors.white,
                  ),
                  SizedBox(width: getProportionateScreenWidth(4)),
                  Flexible(
                    child: Text(
                      "Votre proposition : " +
                          widget.pendingObject.proposition.toString() +
                          "\$",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Jost'),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                    top: getProportionateScreenWidth(3),
                    bottom: getProportionateScreenWidth(3)),
                padding: EdgeInsets.all(getProportionateScreenWidth(2)),
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(4)),
                child: Text("En cour de traitement",
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(14),
                        color: Colors.white,
                        fontFamily: 'Jost',
                        fontWeight: FontWeight.bold)),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(
                      top: getProportionateScreenWidth(6),
                      bottom: getProportionateScreenWidth(6)),
                  padding: EdgeInsets.all(getProportionateScreenWidth(4)),
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        offset: const Offset(0.0, 5.0), //(x,y)
                        blurRadius: 7.0,
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.cancel_presentation_sharp,
                        size: getProportionateScreenWidth(18),
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: getProportionateScreenWidth(5),
                      ),
                      Text("J'annule ma proposition",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(14),
                              color: Colors.white,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Icon(
                    Icons.emoji_people,
                    size: getProportionateScreenWidth(20),
                    color: Colors.white,
                  ),
                  Text(
                    widget.pendingObject.places.toString() + ' Places',
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(12),
                        color: Colors.white70,
                        fontFamily: 'Jost'),
                  ),
                ],
              ),
              SizedBox(height: getProportionateScreenWidth(3)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse de départ",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.pendingObject.departureAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse d'arrivé",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.pendingObject.arrivalAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.watch_later_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Date de la course",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.pendingObject.reservationDate +
                              ' ' +
                              widget.pendingObject.reservationTime,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.note,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Text(
                      widget.pendingObject.note.replaceAll('<br>', '').replaceAll('<b>', '').replaceAll('</b>', ''),
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(12),
                          color: Colors.white,
                          fontFamily: 'Jost'),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
            ],
          )),
          Padding(
            padding: EdgeInsets.only(
                left: getProportionateScreenWidth(10),
                right: getProportionateScreenWidth(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: [
                    const Icon(
                      Icons.map_sharp,
                      color: Colors.white,
                    ),
                    Text(
                      widget.pendingObject.distance.toString() + " Km",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontFamily: 'Jost'),
                    ),
                  ],
                ),
                Text(
                  'Distance',
                  style: TextStyle(
                      fontSize: getProportionateScreenWidth(14),
                      color: Colors.white70,
                      fontFamily: 'Jost'),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
