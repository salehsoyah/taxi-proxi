import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/appbar.dart';
import 'package:taxi_proxi/views/pending_proposals/components/body.dart';

class ProposalsPending extends StatefulWidget {
  const ProposalsPending({Key? key}) : super(key: key);

  @override
  _ProposalsPendingState createState() => _ProposalsPendingState();
}

class _ProposalsPendingState extends State<ProposalsPending> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      appBar: Appbar(),
      body: Body(),
    );
  }
}
