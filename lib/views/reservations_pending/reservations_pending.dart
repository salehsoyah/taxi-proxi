import 'package:flutter/material.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/appbar.dart';
import 'package:taxi_proxi/views/reservations_pending/components/body.dart';

class ReservationsPending extends StatefulWidget {
  const ReservationsPending({Key? key}) : super(key: key);

  @override
  _ReservationsPendingState createState() => _ReservationsPendingState();
}

class _ReservationsPendingState extends State<ReservationsPending> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      appBar: Appbar(),
      body: Body(),
    );
  }
}
