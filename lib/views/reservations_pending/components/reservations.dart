import 'package:flutter/material.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/reservations_pending/components/reservation_card.dart';

class Reservations extends StatefulWidget {
  Reservations({Key? key, required this.reservations}) : super(key: key);
  List<Reservation> reservations;

  @override
  _ReservationsState createState() => _ReservationsState();
}

class _ReservationsState extends State<Reservations> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.all(getProportionateScreenWidth(8)),
        child: ListView.builder(
            itemCount: widget.reservations.length,
            itemBuilder: (BuildContext context, int index) {
              return ReservationCard(
                press: () {},
                reservation: widget.reservations[index],
              );
            }),
      ),
    );
  }
}
