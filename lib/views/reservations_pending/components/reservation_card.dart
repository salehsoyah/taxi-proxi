import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:taxi_proxi/models/errors.dart';
import 'package:taxi_proxi/models/reservation.dart';
import 'package:taxi_proxi/utilities/constants.dart';
import 'package:taxi_proxi/utilities/size_config.dart';
import 'package:taxi_proxi/views/common/default_button.dart';
import 'package:taxi_proxi/views/common/form_error.dart';

class ReservationCard extends StatefulWidget {
  final GestureTapCallback press;
  final Reservation reservation;

  const ReservationCard(
      {Key? key, required this.press, required this.reservation})
      : super(key: key);

  @override
  _ReservationCardState createState() => _ReservationCardState();
}

class _ReservationCardState extends State<ReservationCard> {
  final _formKey = GlobalKey<FormState>();

  void addError(String error) {
    List<String?> errors = Provider.of<Error>(context, listen: false).errors;
    if (!errors.contains(error)) {
      setState(() {
        Provider.of<Error>(context, listen: false).addError(error);
      });
    }
  }

  void removeError(String error) {
    List<String?> errors = Provider.of<Error>(context, listen: false).errors;
    if (errors.contains(error)) {
      setState(() {
        Provider.of<Error>(context, listen: false).removeError(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(getProportionateScreenWidth(10)),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
        gradient: const LinearGradient(
            colors: [Color(0xFFd00000), Color(0xFFd00000)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            tileMode: TileMode.clamp),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            offset: const Offset(0.0, 5.0), //(x,y)
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: getProportionateScreenWidth(8)),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  const Icon(
                    Icons.directions_car,
                    color: Colors.white,
                  ),
                  SizedBox(width: getProportionateScreenWidth(4)),
                  Flexible(
                    child: Text(
                      widget.reservation.value,
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Jost'),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) {
                      return alertDialog();
                    },
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(
                      top: getProportionateScreenWidth(6),
                      bottom: getProportionateScreenWidth(6)),
                  padding: EdgeInsets.all(getProportionateScreenWidth(4)),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        offset: const Offset(0.0, 5.0), //(x,y)
                        blurRadius: 7.0,
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.directions_car,
                        size: getProportionateScreenWidth(18),
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: getProportionateScreenWidth(5),
                      ),
                      Text("Soumettre une proposition",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(14),
                              color: Colors.white,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Icon(
                    Icons.emoji_people,
                    size: getProportionateScreenWidth(20),
                    color: Colors.white,
                  ),
                  Text(
                    widget.reservation.places.toString() + ' Places',
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(12),
                        color: Colors.white70,
                        fontFamily: 'Jost'),
                  ),
                ],
              ),
              SizedBox(height: getProportionateScreenWidth(3)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse de départ",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.reservation.departureAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.flag_sharp,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Adresse de retour",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.reservation.arrivalAddress,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.watch_later_sharp,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Date de la course",
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white70,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          widget.reservation.reservationDate + ' ' + widget.reservation.reservationTime,
                          style: TextStyle(
                              fontSize: getProportionateScreenWidth(12),
                              color: Colors.white,
                              fontFamily: 'Jost'),
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.note,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(3),
                  ),
                  Flexible(
                    child: Text(
                      widget.reservation.note.replaceAll('<br>', '').replaceAll('<b>', '').replaceAll('</b>', ''),
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(12),
                          color: Colors.white,
                          fontFamily: 'Jost'),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(5),
              ),
            ],
          )),
          Padding(
            padding: EdgeInsets.only(
                left: getProportionateScreenWidth(10),
                right: getProportionateScreenWidth(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: [
                    const Icon(
                      Icons.map_sharp,
                      color: Colors.white,
                    ),
                    Text(
                      widget.reservation.distance.toString() + " Km",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(18),
                          color: Colors.white,
                          fontFamily: 'Jost'),
                    ),
                  ],
                ),
                Text(
                  'Distance',
                  style: TextStyle(
                      fontSize: getProportionateScreenWidth(14),
                      color: Colors.white70,
                      fontFamily: 'Jost'),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget alertDialog() {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Align(
                alignment: Alignment.topLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Container(
                      padding: const EdgeInsets.all(2),
                      color: const Color(0xFFd00000),
                      child: const Icon(
                        Icons.clear,
                        color: Colors.white,
                      )),
                )),
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text("Soumettre une proposition",
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Jost',
                  fontWeight: FontWeight.bold,
                  fontSize: getProportionateScreenWidth(20))),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text(
            "La proposition doit être au moin 20\$.",
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Jost',
                fontSize: getProportionateScreenWidth(16)),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          Form(
            key: _formKey,
            child: TextFormField(
              keyboardType: TextInputType.number,
              onChanged: (value) {
                if (value.isNotEmpty) {
                  removeError(kPropositionNullError);
                }

                return;
              },
              validator: (value) {
                if (value!.isEmpty) {
                  addError(kPropositionNullError);
                  return "";
                }
                return null;
              },
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: const BorderSide(
                    color: Colors.amber,
                  ),
                ),
                labelText: "Proposition",
                hintText: "Entrer votre Proposition",
                labelStyle: const TextStyle(fontFamily: 'Jost'),
                floatingLabelBehavior: FloatingLabelBehavior.always,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
            ),
          ),
          Consumer<Error>(
            builder: (context, errors, child) {
              return FormError(errors: errors.errors);
            },
          ),
          SizedBox(
            height: getProportionateScreenHeight(15),
          ),
          DefaultButton(
            text: "Confirmer",
            press: () {
              if (_formKey.currentState!.validate()) {}
            },
          ),
        ],
      ),
    );
  }
}
